import Vue from 'vue';
import App from './App.vue'
import VueRouter from 'vue-router';
import { routes } from './routes.js';
import * as VueGoogleMaps from 'vue2-google-maps'
import VueFullPage from '../node_modules/vue-fullpage.js'
import '../node_modules/vue-fullpage.js/'


import '../src/assets/css/site.css';
import '../src/assets/css/fullpage.min.css'
import '../node_modules/material-icons/iconfont/material-icons.css'
Vue.use(VueRouter);
Vue.use(VueFullPage);

 
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC2tLV76TLS28MbEkiH1gURV7Mh4WvZIPg',
    libraries: 'places' 
  },

  installComponents: true
})

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.config.productionTip = false;


new Vue({
  router,
  render: h => h(App),
  

}).$mount('#app')