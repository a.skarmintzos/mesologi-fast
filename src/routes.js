import Approach from '../src/components/approach.vue';
import Services from '../src/components/services.vue';
import Index from '../src/components/Index.vue';
import Contact from '../src/components/Contact.vue';


export const routes = [
    {path: '/', component: Index},
    {path: '/approach', component: Approach},
    {path: '/services', component: Services},
    {path: '/contact', component: Contact}
];
    